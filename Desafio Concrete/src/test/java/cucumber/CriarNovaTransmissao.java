package test.java.cucumber;

import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.FileSystems;

public class CriarNovaTransmissao {

    public CriarNovaTransmissao() throws MalformedURLException {
    }

    private static AndroidDriver driver;
    private static DesiredCapabilities capacidades;

    @Before
    public void setUp() throws Exception {

        if (driver == null) {

            File diretorio = new File(getPathDriver());
            File app = new File(diretorio, "com.whatsapp2.apk");

            capacidades = new DesiredCapabilities();
            capacidades.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());
            capacidades.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Emulator");
            capacidades.setCapability(MobileCapabilityType.PLATFORM, MobilePlatform.ANDROID);

            driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capacidades);
        }
    }

    @After
    public void tearDown() throws Exception {

        if (driver != null) {

            driver.quit();

        }
    }

    @Dado("^que estou na tela com a lista de conversas$")
    public void message_list() throws Throwable {

        Thread.sleep(10000);
    }

    @Quando("^clicar acessar o menu$")
    public void menu_access() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }
    @E("^clicar no opção nova transmissão$")
    public void click_new_broadcast_option() throws  Throwable{
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }
    @E("^selecionar dois ou mais contatos$")
    public void contact_select() throws  Throwable{
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }
    @E("^clicar no botão confirmar$")
    public void click_confirm_button() throws  Throwable{
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }
    @Então("^é exibida a tela da nova transissão informando a quantidade de destinatários e as opções para envio da mensagem\\.$")
    public void new_broadcast_chat() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    public static String getPathDriver(){

        final String separator = FileSystems.getDefault().getSeparator();

        StringBuilder builder = new StringBuilder();
        builder.append("files");
        builder.append(separator);
        return builder.toString();
    }
}
