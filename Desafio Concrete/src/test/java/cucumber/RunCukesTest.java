package test.java.cucumber;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(format = {"pretty", "html:target/Destination/mobile"},
                 features = {"src\\test\\java\\cucumber/features"})
public class RunCukesTest {

}