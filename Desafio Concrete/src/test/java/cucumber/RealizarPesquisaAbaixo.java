package test.java.cucumber;

import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.FileSystems;

public class RealizarPesquisaAbaixo {

    public RealizarPesquisaAbaixo() throws MalformedURLException {
    }

    private static AndroidDriver driver;
    private static DesiredCapabilities capacidades;

    @Before
    public void setUp() throws Exception {

        if (driver == null) {

            File diretorio = new File(getPathDriver());
            File app = new File(diretorio, "com.whatsapp2.apk");

            capacidades = new DesiredCapabilities();
            capacidades.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());
            capacidades.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Emulator");
            capacidades.setCapability(MobileCapabilityType.PLATFORM, MobilePlatform.ANDROID);

            driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capacidades);
        }
    }

    @After
    public void tearDown() throws Exception {

        if (driver != null) {

            driver.quit();

        }
    }

    @Dado("^que estou na tela do contato$")
    public void contact_list() throws Throwable {

        Thread.sleep(10000);
    }
    @Quando("^clicar na opção de menu$")
    public void menu_access() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }
    @E("^clicar na opção pesquisar$")
    public void click_search_option() throws  Throwable{
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }
    @E("^digitar a letra, palavra ou texto$")
    public void write_words() throws  Throwable{
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }
    @E("^clicar no botão a baixo$")
    public void click_to_down_button() throws  Throwable{
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Então("^É exibida a tela o primeiro resultado encontrado a cima da mensagem selecionada\\.$")
    public void location_word() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    public static String getPathDriver(){

        final String separator = FileSystems.getDefault().getSeparator();

        StringBuilder builder = new StringBuilder();
        builder.append("files");
        builder.append(separator);
        return builder.toString();
    }
}
