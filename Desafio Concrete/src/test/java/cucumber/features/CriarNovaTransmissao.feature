    # language: pt
Funcionalidade: Criar nova transmissão para contatos diferentes.
 Cenário: Enviar uma mesma mensagem, através do menu nova transmissão enviando o mesmo conteúdo para usuários diferentes simultaneamente, que podem ou não estar em um grupo em comum.
 Dado que estou na tela com a lista de conversas
 Quando clicar na opção de menu
  E clicar na opção Nova Transmissão
  E selecionar dois ou mais contatos
  E clicar no botão confirmar
  Então  É exibida a tela da nova transissão informando a quantidade de destinatários e as opções para envio da mensagem.
