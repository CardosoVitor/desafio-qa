# language: pt
Funcionalidade: Realizar pesquisa de textos a baixo dentro de uma conversa
  Cenário: Através da tela de conversa de um contato, trasmissão ou grupo, posso realizar busca de palavras existentes a baixo da última selecionada nas conversas arquivadas.
    Dado que estou na tela do contato
    Quando clicar na opção de menu
    E clicar na opção pesquisar
    E digitar a letra, palavra ou texto
    E clicar no botão a baixo
    Então  É exibida a tela o primeiro resultado encontrado a cima da mensagem selecionada.
