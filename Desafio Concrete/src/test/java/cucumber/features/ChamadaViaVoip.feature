# language: pt
Funcionalidade: Realizar ligação através via VoIP para contatos da lista.
 Cenário: Através da tela de conversa de um contato, posso realizar uma chamada via VoIP através do comando Chamada de Voz via WhatsApp
 Dado que estou na tela do contato
 Quando clicar no botão Chamada de voz via WhatsApp
 Então  É exibida a tela de ligação com o nome e foto do contato com status chamando.
