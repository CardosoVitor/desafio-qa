# language: pt
Funcionalidade: Realizar venda de 4 itens A com desconto de 10% no valor total
 Cenário: Através da tela de lançamento de item, realizar venda do item A com quantidade = 4
 Dado que estou na tela de lançamento de item
 Quando digito o codigo 1 referente ao Item A
 E digito a quantidade a ser lançada
 Então  É exibida a tela informando o valor total com desconto, o valor do desconto e o valor bruto da vendo do Item A.
