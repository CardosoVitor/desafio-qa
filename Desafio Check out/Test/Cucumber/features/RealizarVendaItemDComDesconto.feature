# language: pt
Funcionalidade: Realizar venda de 5 itens D com desconto de 15% no valor total
 Cenário: Através da tela de lançamento de item, realizar venda do item D com quantidade = 5
 Dado que estou na tela de lançamento de item
 Quando digito o codigo 1 referente ao Item D
 E digito a quantidade a ser lançada
 Então  É exibida a tela informando o valor total com desconto, o valor do desconto e o valor bruto da vendo do Item D.
