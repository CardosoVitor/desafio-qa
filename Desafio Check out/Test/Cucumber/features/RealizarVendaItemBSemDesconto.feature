# language: pt
Funcionalidade: Realizar venda de 5 itens B sem desconto no valor total
 Cenário: Através da tela de lançamento de item, realizar venda do item B com quantidade = 5
 Dado que estou na tela de lançamento de item
 Quando digito o codigo 2 referente ao Item B
 E digito a quantidade a ser lançada
 Então  É exibida a tela informando o valor total bruto do Item B.
