# language: pt
Funcionalidade: Realizar venda de 2 itens C com desconto de 8% no valor total
 Cenário: Através da tela de lançamento de item, realizar venda do item C com quantidade = 2
 Dado que estou na tela de lançamento de item
 Quando digito o codigo 1 referente ao Item C
 E digito a quantidade a ser lançada
 Então  É exibida a tela informando o valor total com desconto, o valor do desconto e o valor bruto da vendo do Item C.
