
import java.util.Scanner;


public class Main {

    static class Produto {

        int codigo;
        String nome;
        double valor;
        double descontoPorcentagem;

        Produto a;
        Produto b;
        Produto c;
        Produto d;

        public Produto() {
        }

        public Produto(int codigo, String nome, double valor, double descontoPorcentagem) {
            this.codigo = codigo;
            this.nome = nome;
            this.valor = valor;
            this.descontoPorcentagem = descontoPorcentagem;
        }

        public void iniciaProduto() {

            this.a = new Produto(1,"Arroz", 4.99, 10);

            this.b = new Produto(2,"Açucar", 6.20, 3);

            this.c = new Produto(3,"Café", 8.35, 8);

            this.d = new Produto(4,"Feijão", 6.79, 15);

        }

        public String getNome() {
            return nome;
        }


        public double getValor() {
            return valor;
        }

        public double getDescontoPorcentagem() {
            return descontoPorcentagem;
        }

        public int getCodigo() {
            return codigo;
        }

        public Produto getA() {
            return a;
        }

        public Produto getB() {
            return b;
        }

        public Produto getC() {
            return c;
        }

        public Produto getD() {
            return d;
        }
    }

    public void tratarVenda(Scanner av, Produto produtoFluxo) {

        double valorTotal = 0.0;

        Produto produto = null;

        boolean codprod = false;
        while (!codprod){
          System.out.println("Digite o código do Produto");
           int retprod = av.nextInt();
            if(retprod <=0  || retprod>=5){ System.out.println("Código do Produto inválido");  }
           switch (retprod){
            case 1:
                produto = produtoFluxo.getA() ;
                codprod = true;
                break;
            case 2:
                produto = produtoFluxo.getB();
                codprod = true;
                break;
            case 3:
                produto =produtoFluxo.getC();
                codprod = true;
                break;
            case 4:
                produto =produtoFluxo.getD();
                codprod = true;
                break;
            default:
                codprod = false;
         }
        }

        boolean cont = false;
        while(!cont) {
            System.out.println("Digite a qnt do(a) " + produto.getNome()+ ":");
            int ret = av.nextInt();
            if(ret <=0 || ret >=6){System.out.println("Quantidade do(a) " + produto.getNome()+ " inválida. Deve ser maior que 0 e menor que 6.");}
            switch (ret) {
                case 1:
                    System.out.println("O Valor do(a) " + produto.getNome() + " é: " + produto.getValor());
                    cont = true;
                    break;
                case 2:
                    if (produto.getCodigo() == 3) {
                        valorTotal = produto.getValor() * 2;
                        double valorDesconto = (valorTotal * produto.descontoPorcentagem) / 100;
                        valorTotal = valorTotal - valorDesconto;
                        System.out.println("O Valor com desconto do produto " + produto.getNome() + " é: R$" + valorTotal + "\n");
                        System.out.println("O produto " + produto.getNome() + " teve desconto de: R$" + valorDesconto + "\n");
                    }
                    valorTotal = produto.getValor() * 2;
                    System.out.println("O Valor bruto do produto " + produto.getNome() + " é: " + valorTotal);
                    cont = true;
                    break;
                case 3:
                    if (produto.getCodigo() == 2) {
                        valorTotal = produto.getValor() * 3;
                        double valorDesconto = (valorTotal * produto.descontoPorcentagem) / 100;
                        valorTotal = valorTotal - valorDesconto;
                        System.out.println("O Valor com desconto do produto " + produto.getNome() + " é: R$" + valorTotal + "\n");
                        System.out.println("O produto " + produto.getNome() + " teve desconto de: R$" + valorDesconto + "\n");
                    }
                    valorTotal = produto.getValor() * 3;
                    System.out.println("O Valor bruto do produto " + produto.getNome() + " é: " + valorTotal);
                    cont = true;
                    break;
                case 4:

                    if (produto.getCodigo() == 1) {
                        valorTotal = produto.getValor() * 4;
                        double valorDesconto = (valorTotal * produto.descontoPorcentagem) / 100;
                        valorTotal = valorTotal - valorDesconto;
                        System.out.println("O Valor com desconto do produto " + produto.getNome() + " é: R$" + valorTotal + "\n");
                        System.out.println("O produto " + produto.getNome() + " teve desconto de: R$" + valorDesconto + "\n");
                    }
                    valorTotal = produto.getValor() * 4;
                    System.out.println("O Valor bruto do produto " + produto.getNome() + " é: " + valorTotal);
                    cont = true;
                    break;
                case 5:
                    if (produto.getCodigo() == 4) {
                        valorTotal = produto.getValor() * 5;
                        double valorDesconto = (valorTotal * produto.descontoPorcentagem) / 100;
                        valorTotal = valorTotal - valorDesconto;
                        System.out.println("O Valor com desconto do produto " + produto.getNome() + " é: R$" + valorTotal + "\n");
                        System.out.println("O produto " + produto.getNome() + " teve desconto de: R$" + valorDesconto + "\n");
                    }
                    valorTotal = produto.getValor() * 5;
                    System.out.println("O Valor bruto do produto " + produto.getNome() + " é : R$" + valorTotal);
                    cont = true;
                    break;
                default:
                   cont = false;
                    break;
            }
      }
    }

    public static void main(String[] args) {
        Scanner av = new Scanner(System.in);
        Scanner optv = new Scanner(System.in);



        Produto produtoFluxo = new Produto();


        Main main = new Main();


        System.out.println("Digite a opção deseja: 1 - Inciar venda |||| 2 - Sair");

        if (optv.nextInt() == 1) {
            produtoFluxo.iniciaProduto();
            main.tratarVenda(av, produtoFluxo);
        } else {
            System.out.println("Obrigado e Volte sempre!");
            System.out.close();
        }

    }
}
